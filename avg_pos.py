import numpy as np

def create_bucket_map():
	# setting it up so that we can easily bin data in .5 intervals
	bins = np.arange(1,8.5,.5)
	bucket_map = {}
	for i in range(1, len(bins)+1):
	    bucket_map[i] = '%.2f - %.2f' % (bins[i-1], bins[i-1]+.49)
	print(bucket_map)
	return (bucket_map, bins)

def find_bucket(avg_pos, bucket_map, bins):
    return bucket_map[np.digitize(avg_pos, bins).flatten()[0]]